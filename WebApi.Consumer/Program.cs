﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace WebApi.Consumer
{
    public class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        static async Task MainAsync()
        {
            Uri baseUri = new Uri("https://localhost:44380/");
            HttpClient http = new HttpClient();
            http.BaseAddress = baseUri;
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            Consumer consumer = new Consumer(http);

            HttpContent content = await consumer.GetAllProductsAsync();
            var result = await content.ReadAsStringAsync();
            Console.WriteLine(result);
            //Product product = JsonConverter.DeserializeObject(result);
            System.Console.ReadKey();

        }
    }
}
