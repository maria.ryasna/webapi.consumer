﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Consumer
{
    public class Consumer
    {
        private HttpClient _http { get; }
        private string productPath = "api/products";
        public Consumer(HttpClient http)
        {
            _http = http ?? throw new ArgumentNullException(nameof(http));
        }

        public async Task<HttpContent> GetProductByIdAsync(string id)
        {
            HttpResponseMessage response = await _http.GetAsync($"{productPath}/{id}");
            if (response.IsSuccessStatusCode)
            {
                return response.Content;
            }
            return null;
        }

        public async Task<HttpContent> GetAllProductsAsync()
        {
            HttpResponseMessage response = await _http.GetAsync($"{productPath}");
            if (response.IsSuccessStatusCode)
            {
                return response.Content;
            }
            return null;
        }

        public async Task<bool> DeleteProductByIdAsync()
        {
            HttpResponseMessage response = await _http.DeleteAsync($"{productPath}");

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateProductAsync(Product product)
        {
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            HttpResponseMessage response = await _http.PutAsync($"{productPath}/{product.ProductId}", byteContent);
            response.EnsureSuccessStatusCode();

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> AddProductAsync(Product product)
        {
            var myContent = JsonConvert.SerializeObject(product);

            HttpContent c = new StringContent(myContent, Encoding.UTF8, "application/json");
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            HttpResponseMessage response = await _http.PostAsync($"{productPath}", c);
            response.EnsureSuccessStatusCode();

            return response.IsSuccessStatusCode;
        }
    }
}
